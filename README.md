# Recoveryone Interview Project
The goal of this project is to create a simple but effective pig-latin translator
that will translate most phrases while maintaining capitalization and punctuation.

## Running the Script
```
  ruby translator.rb "<phrase to be translated>"
```

## Running the Specs
Rspec is being used as the test suite. Be sure to install it with 
```
bundle install
```

Specs can be run all at once with
```
rspec spec
```
Specs can also be run individually with
```
rspec spec/<spec.rb>
```
For example, the translator spec can be run with 
```
rspec spec/translator_spec.rb
```
