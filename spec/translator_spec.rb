require_relative "../translator"

describe Translator do
  subject { Translator.new }
  it "translates phrases to pig latin" do
    expect(subject.translate("hello")).to eq("ellohay")
    expect(subject.translate("eat")).to eq("eatway")
    expect(subject.translate("yellow")).to eq("yellowway")
    expect(subject.translate("eat world")).to eq("eatway orldway")
    expect(subject.translate("Hello")).to eq("Ellohay")
    expect(subject.translate("Apples")).to eq("Applesway")
    expect(subject.translate("eat... world?!")).to eq("eatway... orldway?!")
    expect(subject.translate("school")).to eq("oolschay")
    expect(subject.translate("quick")).to eq("ickquay")
    expect(subject.translate("she's great!")).to eq("e'sshay eatgray!")
    expect(subject.translate("HELLO")).to eq("ELLOHAY")
    expect(subject.translate("Hello There")).to eq("Ellohay Erethay")
  end
end
