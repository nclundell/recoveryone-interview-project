require "pry-byebug"

class Translator
  def translate(phrase)
    return "Please give me a phrase to translate!" if phrase.nil?

    phrase.split(" ").map { |word| translate_word(word) }.join(" ")
  end

  private

  def handle_capitalization(word, partitioned_word)
    case word
    when word.capitalize
      partitioned_word.join.capitalize
    when word.downcase
      partitioned_word.join.downcase
    when word.upcase
      partitioned_word.join.upcase
    end
  end

  def handle_punctuation(partitioned_word)
    partitioned_section = partitioned_word[1].partition(/[,.?!]/)
    partitioned_word[1] = partitioned_section[0]
    partitioned_word.push(partitioned_section[1])
    partitioned_word.push(partitioned_section[2])
  end

  def translate_word(word)
    partitioned_word = word.partition(/[aeioy]|(?<!q)u/i).rotate
    partitioned_word.last == "" ? partitioned_word.push("way") : partitioned_word.push("ay")
    partitioned_word = handle_punctuation(partitioned_word)

    handle_capitalization(word, partitioned_word)
  end
end

puts Translator.new.translate(ARGV[0])
